require 'webrick'

server = WEBrick::HTTPServer.new(:ServerName => "my.host", :Port => ARGV.first)
server.mount_proc '/' do |req, res|
  puts res.status
  puts req.body
end

trap 'INT' do
  server.shutdown
end
server.start